import React, { Component } from "react";
import Login from "./../components/login";
import Home from "../components/Home";
import { createStackNavigator } from "@react-navigation/stack";

const RootStack = createStackNavigator();

const RootStackScreen = ({ navigation }) => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen
      name="Login"
      component={Login}
      options={{ title: "Đăng nhập" }}
    />
    <RootStack.Screen
      name="Home"
      component={Home}
      options={{ title: "Danh sách đơn vị" }}
    />
  </RootStack.Navigator>
);

export default RootStackScreen;
