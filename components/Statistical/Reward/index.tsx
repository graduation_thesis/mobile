import React, { Component } from "react";
import { StyleSheet, View, Dimensions, Button } from "react-native";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from "react-native-chart-kit";
import { showMessage, hideMessage } from "react-native-flash-message";
import FlashMessage from "react-native-flash-message";
import CardItemBordered from './card'
import {
  Container,
  Header,
  Body,
  Content,
  Card,
  CardItem,
  Text,
  Icon,
  Right,
} from "native-base";

class Chart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true,
      dates: [],
      values: [],
    };
  }

  componentDidMount() {
    var keyDates: string[] = [];
    var ValByDates: any[] = [];

    fetch("http://192.168.1.4:8000/api/mobile/chartPersonalReward")
      .then((response) => response.json())
      .then((json) => {
        this.setState({ data: json.data });
        Object.keys(json.data).forEach(function (key) {
          keyDates.push(key);
          ValByDates.push(json.data[key]);
        });
        this.setState({ dates: keyDates, values: ValByDates });
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }
  render() {
    const { data, isLoading, dates, values } = this.state;

    let years = ["0"];
    let vals = [0];

    Object.keys(dates).map((key) => {
      years.push(dates[key]);
    });
    Object.keys(values).map((key) => {
      vals.push(values[key]);
    });
    // vals.splice(1, 1);
    // console.log(years);
    // console.log(vals);
    return (
      <View style={styles.container}>
        <View>
          <LineChart
            onDataPointClick={({ value, dataset, getColor }) =>
              showMessage({
                message: "Giá trị số liệu: " + value,
                type: "info",
                animationDuration: 1000
              })
            }
            data={{
              labels: years,
              datasets: [
                {
                  data: vals,
                },
              ],
            }}
            width={Dimensions.get("window").width} // from react-native
            height={220}
            // yAxisLabel="$"
            yAxisSuffix=" N`"
            yAxisInterval={1} // optional, defaults to 1
            chartConfig={{
              backgroundColor: "#000080",
              backgroundGradientFrom: "#00CED1",
              backgroundGradientTo: "#ffa726",
              decimalPlaces: 2, // optional, defaults to 2dp
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              style: {
                borderRadius: 16,
              },
              propsForDots: {
                r: "3",
                strokeWidth: "2",
                stroke: "#ffa726",
              },
            }}
            bezier
            style={{
              marginVertical: 25,
              borderRadius: 0,
            }}
          />
          <FlashMessage position="top" />
        </View>
        <CardItemBordered />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
  },
});

export default Chart;