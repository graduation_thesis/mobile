import React, { Component } from "react";
import {
  Container,
  Header,
  Body,
  Content,
  Card,
  CardItem,
  Text,
  // Icon,
  Right,
  Left,
  View,
} from "native-base";
import { SafeAreaView, FlatList, StyleSheet } from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

export default class CardItemBordered extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true,
      dates: [],
      values: [],
    };
  }
  componentDidMount() {
    var keyDates: string[] = [];
    var ValByDates: any[] = [];

    fetch("http://192.168.1.4:8000/api/mobile/chartRiskDuty")
      .then((response) => response.json())
      .then((json) => {
        this.setState({ data: json.data });
        Object.keys(json.data).forEach(function (key) {
          keyDates.push(key);
          ValByDates.push(json.data[key]);
        });
        this.setState({ dates: keyDates, values: ValByDates });
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { data, isLoading, dates, values } = this.state;

    return (
      <Container>
        <Content padder>
          <Card>
            <CardItem header bordered>
              <Text style={{ width: "100%", textAlign: "center" }}>
                SỐ LIỆU THỐNG KÊ
              </Text>
            </CardItem>

            <CardItem>
              <Left>
                <View>
                  {/* <SafeAreaView> */}
                    <FlatList
                      data={dates}
                      renderItem={({ item }) => (
                        <Text>
                          <FontAwesome5
                            name={"clock"}
                            size={20}
                            color="#009387"
                          />{" "}
                          {item}
                        </Text>
                      )}
                      keyExtractor={(item, index) => index.toString()}
                    />
                  {/* </SafeAreaView> */}
                </View>
              </Left>
              <Right>
                <View>
                  <FlatList
                    data={values}
                    renderItem={({ item }) => (
                      <Text>
                        {item}{" "}
                        <FontAwesome5 name={"male"} size={20} color="#009387" />
                      </Text>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>
              </Right>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
