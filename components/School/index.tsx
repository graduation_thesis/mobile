import React, { Component } from "react";
import {
  ActivityIndicator,
  ScrollView,
  FlatList,
  Text,
  View,
  Button,
  StyleSheet,
} from "react-native";
import CategoryListItem from "./list";
import Loading from "../Loading";

export default class School extends Component {
    constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: false,
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    fetch("http://192.168.1.4:8000/api/mobile/school")
      .then((response) => response.json())
      .then((json) => {
        this.setState({ loading: false, data: json });
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { data, isLoading } = this.state;
    if (this.state.loading) {
      return (
        <Loading />
      );
    }
    return (
      <FlatList data={data}
            renderItem={({item}) => <CategoryListItem category={item} />}
            // keyExtractor={({ id }, index) => id}
            keyExtractor = { (item, index) => index.toString() }
            contentContainerStyle={{paddingLeft: 16, paddingRight: 16}}
          />
    );
  }
}