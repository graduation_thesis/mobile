import React, { Component } from "react";
import {
  ActivityIndicator,
  Text,
  View,
  StyleSheet,
} from "react-native";

export default class Loading extends Component {
  render() {
      return (
        <View style={styles.container}>
          <ActivityIndicator />
          <Text style={styles.txtLoading}>Đang tải! vui lòng chờ...</Text>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch",
    backgroundColor: "#fff",
    justifyContent: "center",
    paddingLeft: 16,
    paddingRight: 16
  },
  txtLoading: {
    textAlign: 'center'
  },
});
