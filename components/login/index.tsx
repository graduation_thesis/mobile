import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  AsyncStorage,
} from "react-native";

import { AuthContext } from "./../context";

const Login = () => {
  const { signIn } = React.useContext(AuthContext);

  return (
    <View style={styles.container}>
      <View style={styles.container_Image}>
        <Image source={require("../../public/logo/dhcs.png")} />
      </View>
      <View style={styles.container_Input}>
        <TextInput
          placeholder={"Tài khoản"}
          style={styles.input}
          autoCapitalize="none"
        />
        <TextInput
          placeholder={"Mật khẩu"}
          secureTextEntry={true}
          style={styles.input}
        />
        <TouchableOpacity style={styles.button} onPress={() => signIn()}>
          <Text style={styles.buttonText}>Đăng nhập</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.TextCont}>
        <Text>&copy; 2020 - Đại học Cảnh sát Nhân dân</Text>
      </View>
    </View>
  );
};

export default Login;

// class Login extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       username: "",
//       password: "",
//     };
//   }
//   componentDidMount() {
//     this._loadInitialState().done();
//   }
//   _loadInitialState = async () => {
//     var value = await AsyncStorage.getItem("user");
//     if (value !== null) {
//       this.props.navigation.navigate("Home");
//     }
//   };
//   render() {
//     return (
//       <View style={styles.container}>
//         <View style={styles.container_Image}>
//           <Image source={require("../../public/logo/dhcs.png")} />
//         </View>
//         <View style={styles.container_Input}>
//           <TextInput
//             placeholder={"Tài khoản"}
//             style={styles.input}
//             autoCapitalize="none"
//             // value={username}
//             onChangeText={(username) => this.setState({ username })}
//           />
//           <TextInput
//             placeholder={"Mật khẩu"}
//             secureTextEntry={true}
//             // value={password}
//             style={styles.input}
//             onChangeText={(password) => this.setState({ password })}
//           />
//           <TouchableOpacity style={styles.button} onPress={this.login}>
//             <Text style={styles.buttonText}>Đăng nhập</Text>
//           </TouchableOpacity>
//         </View>
//         <View style={styles.TextCont}>
//           <Text>&copy; 2020 - Đại học Cảnh sát Nhân dân</Text>
//         </View>
//       </View>
//     );
//   }
//   login = () => {
//     return fetch("http://192.168.1.4:8000/api/auth/login", {
//       method: "POST",
//       headers: {
//         'Accept': "application/json",
//         "Content-Type": "application/json",
//       },
//       body: JSON.stringify({
//         username: this.state.username,
//         password: this.state.password,
//       }),
//     })
//       .then((res) => res.json())
//       .then((resData) => {
//         console.log(resData);
//         if (resData.access_token) {
//           // AsyncStorage.setItem("user", resData.access_token);
//           this.props.navigation.navigate("Home");
//         } else {
//           alert("Vui lòng kiểm tra lại tài khoản hoặc mật khẩu!");
//         }
//       });
//   };
// }

// export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  TextCont: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    marginVertical: 16,
  },

  container_Image: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "flex-end",
  },

  container_Input: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    width: 300,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10,
    borderRadius: 25,
    paddingHorizontal: 30,
    fontSize: 16,
  },
  button: {
    width: 300,
    borderRadius: 25,
    backgroundColor: "#bb0077",
    marginVertical: 18,
    paddingVertical: 12,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    textAlign: "center",
    color: "#FFFFFF",
  },
});
