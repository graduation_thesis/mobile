import React, { Component } from "react";
// import { View, Text, Button, StyleSheet, StatusBar } from 'react-native';
import {
  Container,
  Header,
  Body,
  Content,
  Card,
  CardItem,
  Text,
  Icon,
  Right,
  Left,
  View,
} from "native-base";
import {
  ActivityIndicator,
  StyleSheet
} from "react-native";
import Loading from "./../Loading";

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: false,
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    fetch("http://192.168.1.4:8000/api/mobile/overview")
      .then((response) => response.json())
      .then((json) => {
        this.setState({ loading: false, data: json.data });
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { data, isLoading } = this.state;
    if (this.state.loading) {
      return (
        <Loading />
      );
    }
    return (
      <Container>
        <Content padder>
          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Tài khoản:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[0]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>

          {/* <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Đơn vị trên cơ sở:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>1</Text>
                </View>
              </Right>
            </CardItem>
          </Card> */}

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Đơn vị cơ sở:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[1]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>

          {/* <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Đội, đồn, tổ:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>1</Text>
                </View>
              </Right>
            </CardItem>
          </Card> */}

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Cán bộ:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[2]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Ý kiến phản hồi:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[3]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Khen thưởng cá nhân:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[4]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Khen thưởng tập thể:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[5]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Kỷ luật:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[6]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>

          {/* <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Danh hiệu:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>1</Text>
                </View>
              </Right>
            </CardItem>
          </Card> */}

          {/* <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Hình thức khen thưởng:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>1</Text>
                </View>
              </Right>
            </CardItem>
          </Card> */}

          {/* <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Hình thức kỷ luật:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>1</Text>
                </View>
              </Right>
            </CardItem>
          </Card> */}

          {/* <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Hình thức rủi ro:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>1</Text>
                </View>
              </Right>
            </CardItem>
          </Card> */}

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Thi đua cá nhân chưa duyệt:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[7]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>

          {/* <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Thi đua cá nhân đã duyệt:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>1</Text>
                </View>
              </Right>
            </CardItem>
          </Card> */}

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Thi đua tập thể chưa duyệt:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[8]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>

          {/* <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Thi đua tập thể đã duyệt:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>1</Text>
                </View>
              </Right>
            </CardItem>
          </Card> */}

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Số quyết định:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[9]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Cơ quan quyết định:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[10]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Văn bản:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data[11]}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}