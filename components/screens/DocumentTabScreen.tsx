import React from "react";

import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import DocumentScreen from "./../Document";
import DecisionScreen from './../Decision';
import DecisionAgencyScreen from './../DecisionAgency';

const DocumentStack = createStackNavigator();
const DecisionStack = createStackNavigator();
const DecisionAgencyStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
  <Tab.Navigator
    initialRouteName="Document"
    activeColor="#fff"
    barStyle={{ backgroundColor: "#009387" }}
  >
    <Tab.Screen
      name="Document"
      component={DocumentStackScreen}
      options={{
        tabBarLabel: "Văn bản",
        tabBarColor: "#009387",
        tabBarIcon: ({ color }) => (
          <Icon name="home" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Decision"
      component={DecisionStackScreen}
      options={{
        tabBarLabel: "Quyết định",
        tabBarColor: "#1f65ff",
        tabBarIcon: ({ color }) => (
          <Icon name="library" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="DecisionAgency"
      component={DecisionAgencyStackScreen}
      options={{
        tabBarLabel: "Cơ quan quyết định",
        tabBarColor: "#1f65ff",
        tabBarIcon: ({ color }) => (
          <Icon name="library" color={color} size={26} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default MainTabScreen;

const DocumentStackScreen = ({ navigation }) => (
  <DocumentStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <DocumentStack.Screen
      name="Document"
      component={DocumentScreen}
      options={{
        title: "Văn bản",
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </DocumentStack.Navigator>
);

const DecisionStackScreen = ({ navigation }) => (
  <DecisionStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <DecisionStack.Screen
      name="Quyết định"
      component={DecisionScreen}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </DecisionStack.Navigator>
);

const DecisionAgencyStackScreen = ({ navigation }) => (
    <DecisionAgencyStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: "#009387",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontWeight: "bold",
        },
      }}
    >
      <DecisionAgencyStack.Screen
        name="Cơ quan quyết định"
        component={DecisionAgencyScreen}
        options={{
          headerLeft: () => (
            <Icon.Button
              name="menu"
              size={25}
              backgroundColor="#009387"
              onPress={() => navigation.openDrawer()}
            ></Icon.Button>
          ),
        }}
      />
    </DecisionAgencyStack.Navigator>
  );