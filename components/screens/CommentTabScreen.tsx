import React from "react";

import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import CommentScreen from "./../Comment";

const CommentStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
  <Tab.Navigator
    initialRouteName="Comment"
    activeColor="#fff"
    barStyle={{ backgroundColor: "#009387" }}
  >
    <Tab.Screen
      name="Comment"
      component={CommentStackScreen}
      options={{
        tabBarLabel: "Ý kiến phản hồi",
        tabBarColor: "#009387",
        tabBarIcon: ({ color }) => (
          <Icon name="home" color={color} size={26} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default MainTabScreen;

const CommentStackScreen = ({ navigation }) => (
  <CommentStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <CommentStack.Screen
      name="Comment"
      component={CommentScreen}
      options={{
        title: "Ý kiến phản hồi",
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </CommentStack.Navigator>
);