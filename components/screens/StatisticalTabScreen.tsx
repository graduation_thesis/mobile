import React from "react";

import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

import RewardScreen from "./../Statistical/Reward";
import DisciplineScreen from './../Statistical/Discipline';
import RiskDutyScreen from './../Statistical/RiskDuty';

const RewardStack = createStackNavigator();
const DisciplineStack = createStackNavigator();
const RiskDutyStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
  <Tab.Navigator
    initialRouteName="Reward"
    activeColor="#fff"
    barStyle={{ backgroundColor: "#009387" }}
  >
    <Tab.Screen
      name="Reward"
      component={RewardStackScreen}
      options={{
        tabBarLabel: "Khen thưởng",
        tabBarColor: "#009387",
        tabBarIcon: ({ color }) => (
          <FontAwesome5 name={"laugh-squint"} size={26} color={color} />
        ),
      }}
    />
    <Tab.Screen
      name="Discipline"
      component={DisciplineStackScreen}
      options={{
        tabBarLabel: "Kỷ luật",
        tabBarColor: "#1f65ff",
        tabBarIcon: ({ color }) => (
          <FontAwesome5 name={"angry"} size={26} color={color} />
        ),
      }}
    />
    <Tab.Screen
      name="RiskDuty"
      component={RiskDutyStackScreen}
      options={{
        tabBarLabel: "Rủi ro",
        tabBarColor: "#1f65ff",
        tabBarIcon: ({ color }) => (
          <FontAwesome5 name={"frown-open"} size={26} color={color} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default MainTabScreen;

const RewardStackScreen = ({ navigation }) => (
  <RewardStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <RewardStack.Screen
      name="Reward"
      component={RewardScreen}
      options={{
        title: "Thống kê khen thưởng",
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </RewardStack.Navigator>
);

const DisciplineStackScreen = ({ navigation }) => (
  <DisciplineStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <DisciplineStack.Screen
      name="Thống kê kỷ luật"
      component={DisciplineScreen}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </DisciplineStack.Navigator>
);

const RiskDutyStackScreen = ({ navigation }) => (
    <RiskDutyStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: "#009387",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontWeight: "bold",
        },
      }}
    >
      <RiskDutyStack.Screen
        name="Thống kê rủi ro"
        component={RiskDutyScreen}
        options={{
          headerLeft: () => (
            <Icon.Button
              name="menu"
              size={25}
              backgroundColor="#009387"
              onPress={() => navigation.openDrawer()}
            ></Icon.Button>
          ),
        }}
      />
    </RiskDutyStack.Navigator>
  );