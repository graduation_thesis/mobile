import React from "react";

import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import PersonalTitleScreen from "./../PersonalTitle";
import CollectiveTitleScreen from './../CollectiveTitle';

const PersonalTitleStack = createStackNavigator();
const CollectiveTitleStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
  <Tab.Navigator
    initialRouteName="PersonalTitle"
    activeColor="#fff"
    barStyle={{ backgroundColor: "#009387" }}
  >
    <Tab.Screen
      name="PersonalTitle"
      component={PersonalTitleStackScreen}
      options={{
        tabBarLabel: "Danh hiệu cá nhân",
        tabBarColor: "#009387",
        tabBarIcon: ({ color }) => (
          <Icon name="home" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="CollectiveTitle"
      component={CollectiveTitleStackScreen}
      options={{
        tabBarLabel: "Danh hiệu tập thể",
        tabBarColor: "#1f65ff",
        tabBarIcon: ({ color }) => (
          <Icon name="library" color={color} size={26} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default MainTabScreen;

const PersonalTitleStackScreen = ({ navigation }) => (
  <PersonalTitleStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <PersonalTitleStack.Screen
      name="School"
      component={PersonalTitleScreen}
      options={{
        title: "Danh hiệu cá nhân",
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </PersonalTitleStack.Navigator>
);

const CollectiveTitleStackScreen = ({ navigation }) => (
  <CollectiveTitleStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <CollectiveTitleStack.Screen
      name="Danh hiệu tập thể"
      component={CollectiveTitleScreen}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </CollectiveTitleStack.Navigator>
);