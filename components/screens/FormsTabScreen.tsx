import React from "react";

import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import FormsOfRewardScreen from "./../FormsOfReward";
import FormsOfDisciplineScreen from './../FormsOfDiscipline';
import FormsRiskScreen from './../FormsRisk';

const FormsOfRewardStack = createStackNavigator();
const FormsOfDisciplineStack = createStackNavigator();
const FormsRiskStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
  <Tab.Navigator
    initialRouteName="FormsOfReward"
    activeColor="#fff"
    barStyle={{ backgroundColor: "#009387" }}
  >
    <Tab.Screen
      name="FormsOfReward"
      component={FormsOfRewardStackScreen}
      options={{
        tabBarLabel: "Hình thức khen thưởng",
        tabBarColor: "#009387",
        tabBarIcon: ({ color }) => (
          <Icon name="home" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="FormsOfDiscipline"
      component={FormsOfDisciplineStackScreen}
      options={{
        tabBarLabel: "Hình thức kỷ luật",
        tabBarColor: "#1f65ff",
        tabBarIcon: ({ color }) => (
          <Icon name="library" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="FormsRisk"
      component={FormsRiskStackScreen}
      options={{
        tabBarLabel: "Hình thức rủi ro",
        tabBarColor: "#1f65ff",
        tabBarIcon: ({ color }) => (
          <Icon name="library" color={color} size={26} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default MainTabScreen;

const FormsOfRewardStackScreen = ({ navigation }) => (
  <FormsOfRewardStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <FormsOfRewardStack.Screen
      name="School"
      component={FormsOfRewardScreen}
      options={{
        title: "Hình thức khen thưởng",
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </FormsOfRewardStack.Navigator>
);

const FormsOfDisciplineStackScreen = ({ navigation }) => (
  <FormsOfDisciplineStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <FormsOfDisciplineStack.Screen
      name="Hình thức kỷ luật"
      component={FormsOfDisciplineScreen}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </FormsOfDisciplineStack.Navigator>
);

const FormsRiskStackScreen = ({ navigation }) => (
    <FormsRiskStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: "#009387",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontWeight: "bold",
        },
      }}
    >
      <FormsRiskStack.Screen
        name="Hình thức rủi ro"
        component={FormsRiskScreen}
        options={{
          headerLeft: () => (
            <Icon.Button
              name="menu"
              size={25}
              backgroundColor="#009387"
              onPress={() => navigation.openDrawer()}
            ></Icon.Button>
          ),
        }}
      />
    </FormsRiskStack.Navigator>
  );