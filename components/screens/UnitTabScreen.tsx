import React from "react";

import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";

// import Icon from "react-native-vector-icons/Ionicons";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import SchoolScreen from "../School";
import UnitScreen from '../Unit';
import TeamScreen from "../Team";

const SchoolStack = createStackNavigator();
const UnitStack = createStackNavigator();
const TeamStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
  <Tab.Navigator
    initialRouteName="School"
    activeColor="#fff"
    barStyle={{ backgroundColor: "#009387" }}
  >
    <Tab.Screen
      name="School"
      component={SchoolStackScreen}
      options={{
        tabBarLabel: "Đơn vị TCS",
        tabBarColor: "#009387",
        tabBarIcon: ({ color }) => (
          <Icon name="home" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Unit"
      component={UnitStackScreen}
      options={{
        tabBarLabel: "Đơn vị cơ sở",
        tabBarColor: "#1f65ff",
        tabBarIcon: ({ color }) => (
          <Icon name="library" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Team"
      component={TeamStackScreen}
      options={{
        tabBarLabel: "Đội, đồn, tổ",
        tabBarColor: "#694fad",
        tabBarIcon: ({ color }) => (
          <Icon name="chart-arc" color={color} size={26} />
        ),
      }}
    />

    {/* <Tab.Screen
        name="Unit"
        component={UnitScreen}
        options={{
          tabBarLabel: 'Explore',
          tabBarColor: '#d02860',
          tabBarIcon: ({ color }) => (
            <Icon name="ios-aperture" color={color} size={26} />
          ),
        }}
      /> */}
  </Tab.Navigator>
);

export default MainTabScreen;

const SchoolStackScreen = ({ navigation }) => (
  <SchoolStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <SchoolStack.Screen
      name="School"
      component={SchoolScreen}
      options={{
        title: "Đơn vị trên cơ sở",
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </SchoolStack.Navigator>
);

const UnitStackScreen = ({ navigation }) => (
  <UnitStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <UnitStack.Screen
      name="Đơn vị cơ sở"
      component={UnitScreen}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </UnitStack.Navigator>
);

const TeamStackScreen = ({ navigation }) => (
  <TeamStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#009387",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}
  >
    <TeamStack.Screen
      name="Team"
      component={TeamScreen}
      options={{
        title: "Đội, đồn, tổ",
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#009387"
            onPress={() => navigation.openDrawer()}
          ></Icon.Button>
        ),
      }}
    />
  </TeamStack.Navigator>
);