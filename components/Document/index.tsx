import React, { Component } from "react";
import {
  ActivityIndicator,
  ScrollView,
  FlatList,
  Text,
  View,
  Button,
  StyleSheet,
  TouchableOpacity,
  RefreshControl,
} from "react-native";
import CategoryListItem from "./list";
import { Searchbar } from "react-native-paper";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.state = {
      loading: false, // user list loading
      isRefreshing: false, //for pull to refresh
      data: [], //user list
      error: "",
    };
  }

  componentDidMount() {
    this.fetchUser(this.page);
  }

  fetchUser(page) {
    //stackexchange User API url
    //  const url = `https://api.stackexchange.com/2.2/users?page=${page}&order=desc&sort=reputation&site=stackoverflow`;

    const url = `http://192.168.1.4:8000/api/mobile/document?page=${page}&per_page=10&sort_column=%20id&direction=desc`;

    this.setState({ loading: true });
    fetch(url)
      .then((response) => response.json())
      .then((json) => {
        let listData = this.state.data;
        let data = listData.concat(res.data.data) //concate list with response
        this.setState({ loading: false, data: data });
      })
      .catch((error) => {
        this.setState({ loading: false, error: "Something just went wrong" });
      });
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 2,
          width: "100%",
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };

  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) return null;
    return <ActivityIndicator style={{ backgroundColor: "#000" }} />;
  };

  handleLoadMore = () => {

    if (!this.state.loading) {
      console.log('ok');

      this.page = this.page + 1; // increase page by 1
      this.fetchUser(this.page); // method for API cal  l
    } else {
      console.log('end');
    }
  };

  onRefresh() {
    this.setState({ isRefreshing: true }); // true isRefreshing flag for enable pull to refresh indicator
    const url = `http://192.168.1.4:8000/api/mobile/document?page=1&per_page=10&sort_column=%20id&direction=desc`;
    fetch(url)
      .then((response) => response.json())
      .then((res) => {
        let data = res.data.data;
        this.setState({ isRefreshing: false, data: data }); // false isRefreshing flag for disable pull to refresh indicator, and clear all data and store only first page data
      })
      .catch((error) => {
        this.setState({
          isRefreshing: false,
          error: "Something just went wrong",
        }); // false isRefreshing flag for disable pull to refresh
      });
  }

  render() {
    if (this.state.loading && this.page === 1) {
      return (
        <View
          style={{
            width: "100%",
            height: "100%",
          }}
        >
          <ActivityIndicator style={{ backgroundColor: "#000" }} />
        </View>
      );
    }
    return (
      <View style={{ width: "100%", height: "100%" }}>
        <FlatList
          data={this.state.data}
          extraData={this.state}
          refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={this.onRefresh.bind(this)}
            />
          }
          renderItem={({ item }) => (
            <View
              style={{
                flexDirection: "row",
                padding: 15,
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  fontSize: 18,
                  alignItems: "center",
                  color: "#65A7C5",
                }}
              >
                {item.id}
              </Text>
              <Text
                style={{
                  fontSize: 50,
                  alignItems: "center",
                  color: "#65A7C5",
                }}
              >
                {item.name}
              </Text>
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={this.renderSeparator}
          ListFooterComponent={this.renderFooter.bind(this)}
          onEndReachedThreshold={0.4}
          onEndReached={this.handleLoadMore.bind(this)}
        />
      </View>
    );
  }
}

// import React, { Component } from "react";
// import {
//   ActivityIndicator,
//   ScrollView,
//   FlatList,
//   Text,
//   View,
//   Button,
//   StyleSheet,
//   TouchableOpacity,
//   RefreshControl,
// } from "react-native";
// import CategoryListItem from "./list";
// import { Searchbar } from "react-native-paper";

// export default class Home extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       data: [],
//       isLoading: false,
//       search: "",
//       page: 1,
//     };
//     this.arrayholder = [];
//   }

//   componentDidMount() {
//     this.getData();
//   }

//   getData = () => {
//     this.setState({ isLoading: true });
//     fetch(
//       "http://192.168.1.5:8000/api/mobile/document?page=" +
//         this.state.page +
//         "&per_page=5&sort_column=%20id&direction=desc"
//     )
//       .then((response) => response.json())
//       .then((json) => {
//         this.setState(
//           {
//             isLoading: false,
//             data: json.data.data
//           },
//           function () {
//             this.arrayholder = json.data.data;
//           }
//         );
//       })
//       .catch((error) => console.error(error))
//       .finally(() => {
//         this.setState({ isLoading: false });
//       });
//   };

//   SearchFilterFunction(text) {
//     const newData = this.arrayholder.filter(function (item) {
//       const itemData = item.name ? item.name.toUpperCase() : "".toUpperCase();
//       const textData = text.toUpperCase();
//       return itemData.indexOf(textData) > -1;
//     });
//     this.setState({
//       data: newData,
//       search: text,
//     });
//   }

//   // handleLoadMore = () => {
//   //   this.setState({
//   //     page: this.state.page + 1
//   //   }, () => {
//   //     this.getData()
//   //   })
//   // };

//   handleLoadMore = () => {
//     // this.setState({ page: this.state.page + 1, isLoading: true }, this.getData);
//     fetch(
//       "http://192.168.1.5:8000/api/mobile/document?page=" +
//         (this.state.page+1) +
//         "&per_page=5&sort_column=%20id&direction=desc"
//     )
//       .then((response) => response.json())
//       .then((json) => {
//         // mang = json.data.data;

//         if(json.data.data.length  != 0) {
//           this.setState(
//             {
//               isLoading: false,
//               data: this.state.data.concat(json.data.data),
//               page: this.state.page+1
//             },
//             function () {
//               this.arrayholder = json.data.data;
//             }
//           );
//         } else {
//           alert('đã hết dữ liệu !');
//         }

//       })
//       .catch((error) => console.error(error))
//       .finally(() => {
//         this.setState({ isLoading: false });
//       });
//   };

//   handleRefresh = () => {
//     this.setState({ page: 1, isLoading: true }, this.getData);
//   };

//   // onRefresh() {
//   //   this.setState({ page: 1, isLoading: true }, this.getData);
//   // }

//   renderFooter = () => {
//     return (
//       <View>
//         <ActivityIndicator animating size="large" />
//       </View>
//     );
//   };

//   render() {
//     const { data, isLoading, search } = this.state;

//     if (isLoading) {
//       return (
//         <View
//           style={{
//             flex: 1,
//             padding: 20,
//             alignItems: "center",
//             justifyContent: "center",
//           }}
//         >
//           <ActivityIndicator animating size="large" />
//         </View>
//       );
//     }

//     return (
//       <View>
//         <Searchbar
//           placeholder="Bạn cần tìm..."
//           onChangeText={(text) => this.SearchFilterFunction(text)}
//           value={search}
//         />
//         <FlatList
//           data={data}
//           onEndReached={this.handleLoadMore.bind(this)}
//           ListFooterComponent={this.renderFooter}
//           onEndReachedThreshold={0.5}

//           refreshControl={
//             <RefreshControl refreshing={isLoading} onRefresh={this.getData} />
//           }
//           renderItem={({ item }) => (
//             <TouchableOpacity
//               onPress={() =>
//                 this.props.navigation.navigate("ShowDocument", { item })
//               }
//             >
//               <CategoryListItem category={item} />
//             </TouchableOpacity>
//           )}
//           keyExtractor={(item, index) => index.toString()}
//           contentContainerStyle={{ paddingLeft: 16, paddingRight: 16 }}
//         />
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: "stretch",
//     backgroundColor: "#fff",
//     justifyContent: "center",
//     paddingLeft: 16,
//     paddingRight: 16,
//   },
// });
