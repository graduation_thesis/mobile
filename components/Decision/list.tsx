import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
} from "react-native";
import Moment from 'moment';
import { TouchableOpacity } from "react-native-gesture-handler";

export default function CategoryListItem(props) {
  const { category } = props;
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{category.decision_id}</Text>
      <Text>Ngày kí: {Moment(category.sign_date).format('DD-MM-YYYY')}</Text>
      <Text>Ngày nhận: {Moment(category.received_date).format('DD-MM-YYYY')}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 16,
    borderRadius: 4,
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOpacity: 0.3,
    shadowRadius: 10,
    elevation: 5,
    margin: 5,
    shadowOffset: { width: 8, height: 8 },
  },
  title: {
    textTransform: "uppercase",
    marginBottom: 8,
    fontWeight: "700",
  },
});
