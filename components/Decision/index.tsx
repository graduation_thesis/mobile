import React, { Component } from "react";
import {
  ActivityIndicator,
  ScrollView,
  FlatList,
  Text,
  View,
  Button,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import CategoryListItem from "./list";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true,
    };
  }

  componentDidMount() {
    fetch("http://192.168.1.4:8000/api/mobile/decision")
      .then((response) => response.json())
      .then((json) => {
        this.setState({ data: json });
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { data, isLoading } = this.state;
    return (
      <FlatList
        data={data}
        renderItem={
          ({ item }) => (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ShowDecision', {item})}>
              <CategoryListItem category={item} />
            </TouchableOpacity>
          )
        }
        keyExtractor={(item, index) => index.toString()}
        contentContainerStyle={{ paddingLeft: 16, paddingRight: 16 }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch",
    backgroundColor: "#fff",
    justifyContent: "center",
    paddingLeft: 16,
    paddingRight: 16,
  },
});
