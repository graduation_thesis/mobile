import React, { Component } from "react";
import {
  Container,
  Header,
  Body,
  Content,
  Card,
  CardItem,
  Text,
  // Icon,
  Right,
  Left,
  View,
  Form,
} from "native-base";

import Moment from "moment";

export default class Show extends Component {
  render() {
    const { item } = this.props.route.params;

    return (
      <Container style={{marginTop: 50}}>
        <Content padder>
          <Card>
            <CardItem header bordered>
              <Text style={{ width: "100%", textAlign: "center" }}>
                Quyết định số {item.decision_id}
              </Text>
            </CardItem>
            <CardItem>
                <View>
                  <Text>
                    Ngày kí: {Moment(item.sign_date).format("DD-MM-YYYY")}
                  </Text>
                  <Text>
                    Ngày nhận: {Moment(item.received_date).format("DD-MM-YYYY")}
                  </Text>
                </View>
            </CardItem>
            <CardItem>
              <Body>
              <Text>Trích yếu: {item.description}</Text>
              </Body>
            </CardItem>
            <CardItem>
            </CardItem>
            <CardItem footer bordered>
            <Text>Tệp đính kèm: {item.decision_file}</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

// const styles = StyleSheet.create({
//   container: {
//     alignItems: "center",
//     padding: 16,
//     borderRadius: 4,
//     backgroundColor: "#fff",
//     shadowColor: "#000",
//     shadowOpacity: 0.3,
//     shadowRadius: 10,
//     elevation: 5,
//     margin: 5,
//     shadowOffset: { width: 8, height: 8 },
//   },
//   title: {
//     textTransform: "uppercase",
//     marginBottom: 8,
//     fontWeight: "700",
//   },
// });
