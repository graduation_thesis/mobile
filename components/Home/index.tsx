import React, { Component } from "./node_modules/react";
import {
  ActivityIndicator,
  ScrollView,
  FlatList,
  Text,
  View,
  Button,
  StyleSheet,
} from "react-native";
import CategoryListItem from "./list";

export default class Home extends Component {
    constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true,
    };
  }

  componentDidMount() {
    fetch("http://192.168.1.4:8000/api/test/list")
      .then((response) => response.json())
      .then((json) => {
        this.setState({ data: json.data });
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { data, isLoading } = this.state;
    return (
      <FlatList data={data}
            renderItem={({item}) => <CategoryListItem category={item} />}
            // keyExtractor={({ id }, index) => id}
            keyExtractor = { (item, index) => index.toString() }
            contentContainerStyle={{paddingLeft: 16, paddingRight: 16}}
          />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch",
    backgroundColor: "#fff",
    justifyContent: "center",
    paddingLeft: 16,
    paddingRight: 16
  }
});
