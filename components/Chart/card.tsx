import React, { Component } from "./node_modules/react";
import {
  Container,
  Header,
  Body,
  Content,
  Card,
  CardItem,
  Text,
  Icon,
  Right,
  Left,
  View,
} from "native-base";
export default class CardItemBordered extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      count: 0,
      isLoading: true,
    };
  }
  componentDidMount() {
    fetch("http://192.168.1.4:8000/api/mobile/unit")
      .then((response) => response.json())
      .then((json) => {
        this.setState({ data: json.data.length });
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { data, count, isLoading } = this.state;
    console.log(data);

    return (
      <Container>
        <Content padder>
          <Card>
            <CardItem header bordered>
              <Text>Tổng quan</Text>
            </CardItem>
            <CardItem bordered>
              <Body>
                <Text>Thống kê khen thưởng qua các năm ACE Joker</Text>
              </Body>
            </CardItem>
            <CardItem>
              <Icon active name="logo-googleplus" />
              <Text>Google Plus</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </CardItem>
            <CardItem footer bordered>
              <Text>GeekyAnts</Text>
            </CardItem>
          </Card>

          <Card>
            <CardItem>
              <Left>
                <View>
                  <Text>Số lượng:</Text>
                </View>
              </Left>
              <Right>
                <View>
                  <Text>{data}</Text>
                </View>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Left>
                <Card>
                  <CardItem>
                    <View>
                      <Text>SL: 123</Text>
                    </View>
                  </CardItem>
                </Card>
              </Left>
              <Right>
                <Card>
                  <CardItem>
                    <View>
                      <Text>SL: 1500</Text>
                    </View>
                  </CardItem>
                </Card>
              </Right>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
