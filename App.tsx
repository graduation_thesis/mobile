import React, { Component, useEffect } from "react";

import { createDrawerNavigator } from "@react-navigation/drawer";

import LoginScreen from "./components/login";

import MainTabScreen from "./components/screens/MainTabScreen";
import UnitTabScreen from "./components/screens/UnitTabScreen";
import TitleTabScreen from "./components/screens/TitleTabScreen";
import FormsTabScreen from "./components/screens/FormsTabScreen";
import DocumentTabScreen from "./components/screens/DocumentTabScreen";
import CommentTabScreen from "./components/screens/CommentTabScreen";
import StatisticalTabScreen from "./components/screens/StatisticalTabScreen";
import UserTabScreen from "./components/screens/UserTabScreen";

// TEST SHOW SCREEN
import ShowDecision from "./components/Decision/show";
import ShowDocument from "./components/Document/show";

import {
  NavigationContainer,
  DefaultTheme as NavigationDefaultTheme,
  DarkTheme as NavigationDarkTheme,
} from "@react-navigation/native";

import {
  Provider as PaperProvider,
  DefaultTheme as PaperDefaultTheme,
  DarkTheme as PaperDarkTheme,
} from "react-native-paper";

import { AuthContext } from "./components/context";

import { DrawerContent } from "./components/screens/DrawerContent";

const Drawer = createDrawerNavigator();

const App = () => {
  const [isDarkTheme, setIsDarkTheme] = React.useState(false);

  const CustomDefaultTheme = {
    ...NavigationDefaultTheme,
    ...PaperDefaultTheme,
    colors: {
      ...NavigationDefaultTheme.colors,
      ...PaperDefaultTheme.colors,
      background: "#ffffff",
      text: "#333333",
    },
  };

  const CustomDarkTheme = {
    ...NavigationDarkTheme,
    ...PaperDarkTheme,
    colors: {
      ...NavigationDarkTheme.colors,
      ...PaperDarkTheme.colors,
      background: "#333333",
      text: "#ffffff",
    },
  };

  const theme = isDarkTheme ? CustomDarkTheme : CustomDefaultTheme;

  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken] = React.useState(null);

  const authContext = React.useMemo(
    () => ({
      toggleTheme: () => {
        setIsDarkTheme((isDarkTheme) => !isDarkTheme);
      },

      signIn: () => {
        setUserToken("fgkj");
        setIsLoading(false);
      },
      signOut: () => {
        setUserToken(null);
        setIsLoading(false);
      },
    }),
    []
  );

  return (
    <PaperProvider theme={theme}>
      <AuthContext.Provider value={authContext}>
        <NavigationContainer theme={theme}>
          {userToken !== null ? (
            <Drawer.Navigator
              drawerContent={(props) => <DrawerContent {...props} />}
            >
              <Drawer.Screen
                name="Home"
                component={MainTabScreen}
              ></Drawer.Screen>
              <Drawer.Screen
                name="Unit"
                component={UnitTabScreen}
              ></Drawer.Screen>
              <Drawer.Screen
                name="Title"
                component={TitleTabScreen}
              ></Drawer.Screen>
              <Drawer.Screen
                name="Forms"
                component={FormsTabScreen}
              ></Drawer.Screen>
              <Drawer.Screen
                name="Document"
                component={DocumentTabScreen}
              ></Drawer.Screen>
              <Drawer.Screen
                name="Comment"
                component={CommentTabScreen}
              ></Drawer.Screen>
              <Drawer.Screen
                name="Statistical"
                component={StatisticalTabScreen}
              ></Drawer.Screen>
              <Drawer.Screen
                name="User"
                component={UserTabScreen}
              ></Drawer.Screen>

              <Drawer.Screen
                name="ShowDecision"
                component={ShowDecision}
              ></Drawer.Screen>
              <Drawer.Screen
                name="ShowDocument"
                component={ShowDocument}
              ></Drawer.Screen>
            </Drawer.Navigator>
          ) : (
            <LoginScreen />
          )}
        </NavigationContainer>
      </AuthContext.Provider>
    </PaperProvider>
  );
};
export default App;
